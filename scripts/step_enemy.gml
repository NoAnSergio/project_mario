if (vsp < 10) {
    vsp += grav;
}

//horizontal collision

if (place_meeting(x+hsp,y,obj_gnd)){
    while( !place_meeting(x+sign(hsp),y,obj_gnd)){
        x += sign(hsp);
    }
    hsp = 0;
}
x += hsp;

//vertical

if (place_meeting(x,y+vsp,obj_gnd)){
    while( !place_meeting(x,y+sign(vsp),obj_gnd)){
        y += sign(vsp);
    }
    vsp = 0;
}
y += vsp;
//sprites

image_speed=0.8;
