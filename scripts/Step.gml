//Get player input
key_right = keyboard_check (vk_right);
key_left = -keyboard_check (vk_left);
key_jump = keyboard_check_pressed (vk_space);


//React to inputs
move = key_left + key_right;
hsp = move * movespeed;

if (vsp < 10) {
    vsp += grav;
}

if (place_meeting(x,y+1,obj_gnd)){

    //Check if recently grounded
    if(!grounded && !key_jump){
        hkp_count = 0; //Init horizontal count
        jumping = false;
    }else if(grounded && key_jump){ //recently jumping
        jumping = true;
    }
    
    //Check if player grounded
    grounded = !key_jump;
    vsp = key_jump * -jumpspeed;    
}

//Init hsp_jump_applied
if(grounded){
    hsp_jump_applied = 0;
}

//Check horizontal counts
if(move!=0 && grounded){
 hkp_count++;
}else if(move==0 && grounded){
 hkp_count=0;
}

//Check jumping
if(jumping){
    
    //check if previously we have jump
    if(hsp_jump_applied == 0){
        hsp_jump_applied = sign(move);       
    }

    //don't jump horizontal
    if(hkp_count < hkp_count_small ){
        hsp = 0;
    }else if(hkp_count >= hkp_count_small && hkp_count < hkp_count_big){ //small jump
        hsp = hsp_jump_applied * hsp_jump_constant_small;
    }else{ // big jump
        hsp = hsp_jump_applied *hsp_jump_constant_big
    }
}



//horizontal

if (place_meeting(x+hsp,y,obj_gnd)){
    while( !place_meeting(x+sign(hsp),y,obj_gnd)){
        x += sign(hsp);
    }
    hsp = 0;
}
x += hsp;



//vertical

if (place_meeting(x,y+vsp,obj_gnd)){
    while( !place_meeting(x,y+sign(vsp),obj_gnd)){
        y += sign(vsp);
    }
    vsp = 0;
}
y += vsp;

//sprites


if (hsp>0){
    sprite_index=spr_run;
    image_xscale = 1;
    image_speed=0.8;
    idle=0;
}else if (hsp<0){
    sprite_index=spr_run;
    image_xscale = -1;
    image_speed=0.8;
    idle=0;
}else if (idle<=100){
    sprite_index=spr_player;
    image_speed=0.2;
}

if (grounded==true && idle<=100){
    idle+=1;
}else if (idle>100){
    sprite_index=spr_idle;
    image_speed=0.2;
}

if (vsp!=0){
    sprite_index=spr_jump;
    idle=0;
}
